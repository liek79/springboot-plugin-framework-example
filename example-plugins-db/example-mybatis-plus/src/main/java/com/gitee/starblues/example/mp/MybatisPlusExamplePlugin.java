package com.gitee.starblues.example.mp;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author starBlues
 * @version 1.0
 */
@SpringBootApplication
public class MybatisPlusExamplePlugin extends SpringPluginBootstrap {

    public static void main(String[] args) {
        new MybatisPlusExamplePlugin().run(args);
    }

}
