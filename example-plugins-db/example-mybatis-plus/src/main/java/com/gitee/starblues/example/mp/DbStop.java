package com.gitee.starblues.example.mp;

import com.gitee.starblues.bootstrap.realize.PluginCloseListener;
import com.gitee.starblues.core.descriptor.PluginDescriptor;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
public class DbStop implements PluginCloseListener {

    @Autowired
    private HikariDataSource dataSource;

    @Override
    public void close(PluginDescriptor descriptor) {
        dataSource.close();
    }
}
