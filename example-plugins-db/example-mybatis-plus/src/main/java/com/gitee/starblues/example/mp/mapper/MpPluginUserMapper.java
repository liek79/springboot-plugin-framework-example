package com.gitee.starblues.example.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.starblues.example.mp.entity.MpPluginUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 插件1-用户表 Mapper 接口
 * </p>
 *
 * @author starBlues
 * @since 2021-05-21
 */
@Mapper
public interface MpPluginUserMapper extends BaseMapper<MpPluginUser> {

    MpPluginUser getPluginById(String id);

}
