package com.gitee.starblues.example.mp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.starblues.example.mp.entity.MpPluginUser;
import com.gitee.starblues.example.mp.mapper.MpPluginUserMapper;
import com.gitee.starblues.example.mp.service.MpPluginUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 插件1-用户表 服务实现类
 * </p>
 *
 * @author starBlues
 * @since 2021-05-21
 */
@Service
public class MpPluginUserServiceImpl extends ServiceImpl<MpPluginUserMapper, MpPluginUser> implements MpPluginUserService {

}
