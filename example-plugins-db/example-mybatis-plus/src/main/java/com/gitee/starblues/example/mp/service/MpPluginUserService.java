package com.gitee.starblues.example.mp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.starblues.example.mp.entity.MpPluginUser;
import com.gitee.starblues.example.mp.mapper.MpPluginUserMapper;

/**
 * <p>
 * 插件1-用户表 服务类
 * </p>
 *
 * @author starBlues
 * @since 2021-05-21
 */
public interface MpPluginUserService extends IService<MpPluginUser> {

    @Override
    MpPluginUserMapper getBaseMapper();
}
