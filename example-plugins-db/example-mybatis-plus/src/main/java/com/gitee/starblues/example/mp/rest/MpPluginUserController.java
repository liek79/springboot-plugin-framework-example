package com.gitee.starblues.example.mp.rest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.starblues.example.mp.entity.MpPluginUser;
import com.gitee.starblues.example.mp.service.MpPluginUserService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * user 接口
 * @author starBlues
 * @version 1.0
 * @since 2021-05-25
 */
@RestController
@RequestMapping("/user")
@Api(tags = "mybatis-plus-plugin")
@RequiredArgsConstructor
public class MpPluginUserController {

    private final MpPluginUserService mpPluginUserService;

    @GetMapping
        public Page<MpPluginUser> getPage(@RequestParam("currentPage") Integer currentPage,
                                          @RequestParam("pageSize") Integer pageSize){
        Page<MpPluginUser> plugin2UserPage = new Page<>(currentPage, pageSize);
        return mpPluginUserService.page(plugin2UserPage);
    }


    @GetMapping("/{id}")
    public MpPluginUser getById(@PathVariable("id") String id){
        return mpPluginUserService.getBaseMapper().getPluginById(id);
    }


    @PostMapping
    public MpPluginUser addUser(@RequestBody MpPluginUser plugin2User){
        mpPluginUserService.save(plugin2User);
        return plugin2User;
    }

}
