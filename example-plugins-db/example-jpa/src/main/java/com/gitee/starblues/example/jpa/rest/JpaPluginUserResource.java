package com.gitee.starblues.example.jpa.rest;

import com.gitee.starblues.example.jpa.dao.PluginJpaUserDao;
import com.gitee.starblues.example.jpa.entity.PluginJpaUser;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.List;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/jpa")
@Api(tags = "jap-plugin")
public class JpaPluginUserResource {

    @Resource
    private DataSource dataSource;

    @Resource
    private PluginJpaUserDao pluginJpaUserDao;

    @GetMapping()
    public List<PluginJpaUser> getList(){
        return pluginJpaUserDao.findAll();
    }

}
