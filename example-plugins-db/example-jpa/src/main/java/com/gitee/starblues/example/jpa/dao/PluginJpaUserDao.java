package com.gitee.starblues.example.jpa.dao;

import com.gitee.starblues.example.jpa.entity.PluginJpaUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-05-24
 */
public interface PluginJpaUserDao extends JpaRepository<PluginJpaUser, String> {
}
