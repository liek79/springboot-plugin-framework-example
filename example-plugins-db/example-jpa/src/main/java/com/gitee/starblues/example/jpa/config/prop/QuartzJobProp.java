package com.gitee.starblues.example.jpa.config.prop;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * quartz 的job配置
 * @author starBlues
 * @version 2.4.3
 */
@Component
@ConfigurationProperties(prefix = "job")
@Data
public class QuartzJobProp {

    @Value("${databaseInsertJobCron:*/20 * * * * ?}")
    private String databaseInsertJobCron;

    @Value("${databaseInsertJobDelaySec:0}")
    private Long databaseInsertJobDelaySec;

}
