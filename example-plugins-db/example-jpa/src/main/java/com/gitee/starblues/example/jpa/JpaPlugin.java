package com.gitee.starblues.example.jpa;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * jpa 插件
 * @author starBlues
 * @version 2.4.3
 */
@SpringBootApplication()
public class JpaPlugin extends SpringPluginBootstrap{

    public static void main(String[] args) {
        new JpaPlugin().run(JpaPlugin.class, args);
    }

}
