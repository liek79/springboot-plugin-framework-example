package com.gitee.starblues.example.basic1.service;

import com.gitee.starblues.core.descriptor.PluginDescriptor;
import com.gitee.starblues.example.service.TestAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@Component
@Service
@TestAnnotation
public class BasicService {

    @Autowired
    private PluginDescriptor pluginDescriptor;

    public String getName(){
        return BasicService.class.getName();
    }

}
