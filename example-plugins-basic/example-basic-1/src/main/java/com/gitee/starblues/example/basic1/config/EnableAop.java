package com.gitee.starblues.example.basic1.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@Configuration
@EnableAspectJAutoProxy
public class EnableAop {
}
