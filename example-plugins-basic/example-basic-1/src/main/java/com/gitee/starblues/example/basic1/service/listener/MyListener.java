package com.gitee.starblues.example.basic1.service.listener;


import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;

/**
 * 启动后监听
 * @author starBlues
 * @version 1.0
 */
public class MyListener implements ApplicationListener<ApplicationEvent>{

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ApplicationEnvironmentPreparedEvent) {
            System.out.println("初始化环境变量");
        } else if (event instanceof ApplicationPreparedEvent) {
            System.out.println("环境初始化完成");
        } else if (event instanceof ContextRefreshedEvent) {
            System.out.println("ApplicationContext被刷新");
        } else if (event instanceof ApplicationReadyEvent) {
            System.out.println("插件已经启动完成");
        } else if (event instanceof ContextClosedEvent) {
            System.out.println("插件停止");
        }
    }
}
