package com.gitee.starblues.example.basic1;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import com.gitee.starblues.bootstrap.annotation.DisablePluginWeb;
import com.gitee.starblues.bootstrap.annotation.OneselfConfig;
import com.gitee.starblues.example.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration;
import org.springframework.context.annotation.PropertySource;

import java.net.URL;

/**
 * 基本插件
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@SpringBootApplication
public class Basic1Plugin extends SpringPluginBootstrap {

    public static void main(String[] args) {
        new Basic1Plugin().run(Application.class, args);
    }

}
