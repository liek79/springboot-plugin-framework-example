package com.gitee.starblues.example.basic1.rest;

import com.gitee.starblues.core.descriptor.PluginDescriptor;
import com.gitee.starblues.example.basic1.config.BasicConfig;
import com.gitee.starblues.example.basic1.service.BasicService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@RestController
@RequestMapping("/hello")
@Api(tags = "basic1-plugin-hello")
public class HelloController {

    @Resource
    private BasicService basicService;
    @Resource
    private BasicConfig basicConfig;
    @Resource
    private PluginDescriptor pluginDescriptor;


    @GetMapping()
    public PluginDescriptor hello(){
        return pluginDescriptor;
    }

    @GetMapping("/name")
    public String getName(){
        return basicService.getName();
    }


    @GetMapping("/config")
    public BasicConfig getConfig(){
        return basicConfig;
    }

}
