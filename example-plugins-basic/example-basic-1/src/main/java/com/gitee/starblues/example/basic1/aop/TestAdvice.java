package com.gitee.starblues.example.basic1.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@Component
@Aspect
@Order(10)
@Slf4j
public class TestAdvice {

    @Pointcut("execution(public * com.gitee.starblues.example.basic.service..*.*(..))")
    public void pointCut1() {

    }


    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
    public void pointCut2() {

    }


    @Before(value = "pointCut2()")
    public void controllerBefore(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        log.info("controller aop = 请求URL：" + request.getRequestURL().toString());
    }


    @After(value = "pointCut1()")
    public void serviceAfter(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        log.info("service aop = 请求URL：" + request.getRequestURL().toString());
    }




}
