package com.gitee.starblues.example.basic1.service;

import com.gitee.starblues.annotation.Caller;
import com.gitee.starblues.example.basic1.config.BasicConfig;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author starBlues
 * @version 1.0
 */
@Caller("service-supper")
public interface ServiceCaller {

    @Caller.Method("hello")
    String hello(String s);

    @Caller.Method("getConfig")
    BasicConfig getConfig(String name);

    R param(Param param);

    R paramList(List<Param> params);

    R paramMap(Map<String, Param> params);


    @Data
    class Param{
        private String name;
        private Integer age;
        private List<Param> params;
    }

    @Data
    class R{
        String name;
    }


}
