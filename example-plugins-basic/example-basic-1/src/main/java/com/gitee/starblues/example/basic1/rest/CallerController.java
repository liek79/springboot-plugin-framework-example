package com.gitee.starblues.example.basic1.rest;

import com.gitee.starblues.example.basic1.config.BasicConfig;
import com.gitee.starblues.example.basic1.service.ServiceCaller;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author starBlues
 * @version 1.0
 */
@RestController
@RequestMapping("/caller")
@Api(tags = "basic1-plugin-caller")
public class CallerController {

    @Resource
    private ServiceCaller serviceCaller;

    @GetMapping("/test")
    public BasicConfig caller(){
        String hello = serviceCaller.hello("hello, i am basic");
        System.out.println(hello);
        return serviceCaller.getConfig("hi, i am basic1");
    }

    @GetMapping("/test-param")
    public ServiceCaller.R testParam(){
        List<ServiceCaller.Param> params = new ArrayList<>();
        params.add(get("1", 1, null));
        params.add(get("2", 2, null));
        ServiceCaller.Param param = get("name", 16, params);
        return serviceCaller.param(param);
    }

    @GetMapping("/test-param-list")
    public ServiceCaller.R testParamList(){
        List<ServiceCaller.Param> params = new ArrayList<>();
        params.add(get("1", 1, null));
        params.add(get("2", 2, null));
        return serviceCaller.paramList(params);
    }

    @GetMapping("/test-param-map")
    public ServiceCaller.R testParamMap(){
        Map<String, ServiceCaller.Param> params = new HashMap<>();
        params.put("1", get("1", 1, null));
        params.put("2", get("2", 2, null));
        return serviceCaller.paramMap(params);
    }

    private ServiceCaller.Param get(String name, Integer age, List<ServiceCaller.Param> params){
        ServiceCaller.Param param = new ServiceCaller.Param();
        param.setName("123");
        param.setAge(16);
        param.setParams(params);
        return param;
    }

}
