package com.gitee.starblues.example.basic1.service;

import com.gitee.starblues.example.service.Sender;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
public class Example1Sender implements Sender {
    @Override
    public Name getName() {
        return new Name(Example1Sender.class.getName());
    }
}
