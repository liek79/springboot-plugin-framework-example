package com.gitee.starblues.example.basic1.service;

import com.gitee.starblues.annotation.Extract;
import com.gitee.starblues.example.service.TestAnnotation;
import com.gitee.starblues.example.service.extract.ExtractDemo;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@Extract(bus = "extract", scene = "demo", order = 10)
@TestAnnotation
public class ExtractBasic1 implements ExtractDemo {
    @Override
    public String getName() {
        return "extractBasic1";
    }
}
