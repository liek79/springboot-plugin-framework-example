package com.gitee.starblues.example.basic1.interceptor;

import com.gitee.starblues.bootstrap.processor.interceptor.PluginInterceptorRegister;
import com.gitee.starblues.bootstrap.processor.interceptor.PluginInterceptorRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
public class Basic1InterceptorRegister implements PluginInterceptorRegister {

    @Autowired
    private Basic1HandlerInterceptor basic1HandlerInterceptor;

    @Override
    public void registry(PluginInterceptorRegistry registry) {
        registry.addInterceptor(basic1HandlerInterceptor, PluginInterceptorRegistry.Type.PLUGIN)
                .addPathPatterns("/**");
    }
}
