package com.gitee.starblues.example.basic1.rest;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.UUID;

/**
 * Thymeleaf 模板引擎接口
 * @author starBlues
 * @version 2.3.0
 */
@Controller
@RequestMapping("/thy")
@Api(tags = "basic1-olugin-thymeleaf")
public class ThymeleafController {

    @GetMapping()
    public String show(Model model){
        model.addAttribute("uid", UUID.randomUUID().toString());
        model.addAttribute("name","uuid");
        return "index";
    }


}
