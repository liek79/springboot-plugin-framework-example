package com.gitee.starblues.example.basic1.rest;

import com.gitee.starblues.example.entity.MainUser;
import com.gitee.starblues.example.service.MainUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@RestController
@RequestMapping("/main")
@Api(tags = "basic1-plugin-caller-main")
public class MainUserController {

    @Autowired
    private MainUserService mainUserService;


    @GetMapping("/main-user")
    public List<MainUser> mainUser(){
        return mainUserService.list();
    }

}
