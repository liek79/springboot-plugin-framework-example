package com.gitee.starblues.example.basic2;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 基本插件
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@SpringBootApplication
public class Basic2Plugin extends SpringPluginBootstrap {

    public static void main(String[] args) {
        new Basic2Plugin().run(args);
    }

}
