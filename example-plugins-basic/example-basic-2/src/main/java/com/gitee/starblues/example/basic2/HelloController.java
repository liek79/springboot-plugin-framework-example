package com.gitee.starblues.example.basic2;

import com.gitee.starblues.core.descriptor.PluginDescriptor;
import com.gitee.starblues.example.basic2.config.Basic2Config;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@RestController
@RequestMapping("/hello")
@Api(tags = "basic2-plugin-hello-example")
public class HelloController {

    @Resource
    private Basic2Config basicConfig;
    @Resource
    private PluginDescriptor pluginDescriptor;


    @GetMapping()
    public PluginDescriptor hello(){
        return pluginDescriptor;
    }

    @GetMapping("/config")
    public Basic2Config getConfig(){
        return basicConfig;
    }

}
