package com.gitee.starblues.example.basic2.service;

import com.gitee.starblues.annotation.Extract;
import com.gitee.starblues.example.service.extract.ExtractDemo;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-08-01
 */
@Extract(bus = "extract", scene = "demo", order = 0)
public class ExtractBasic2 implements ExtractDemo {
    @Override
    public String getName() {
        return "extractBasic2";
    }
}
