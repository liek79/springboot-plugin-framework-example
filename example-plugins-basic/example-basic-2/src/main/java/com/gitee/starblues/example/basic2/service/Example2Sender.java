package com.gitee.starblues.example.basic2.service;

import com.gitee.starblues.example.service.Sender;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
public class Example2Sender implements Sender {
    @Override
    public Sender.Name getName() {
        return new Sender.Name(Example2Sender.class.getName());
    }
}
