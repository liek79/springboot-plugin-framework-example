package com.gitee.starblues.example.basic2.service;

import com.gitee.starblues.annotation.Supplier;
import com.gitee.starblues.example.basic2.config.Basic2Config;
import com.gitee.starblues.example.service.TestAnnotation;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * @author starBlues
 * @version 1.0
 */
@Supplier("service-supper")
@TestAnnotation
public class ServiceSupper {

    @Autowired
    private Basic2Config config;

    @Supplier.Method("hello")
    public String hello(String s){
        System.out.println(s);
        return s;
    }

    @Supplier.Method("getConfig")
    public Basic2Config getConfig(String name){
        Basic2Config newConfig = new Basic2Config();
        BeanUtils.copyProperties(config, newConfig);
        newConfig.setName(name);
        return newConfig;
    }

    public R param(Param param){
        System.out.println(param);
        R r = new R();
        r.setName("bean");
        return r;
    }

    public R paramList(List<Param> params){
        System.out.println(params.toString());
        R r = new R();
        r.setName("list");
        return r;
    }

    public R paramMap(Map<String, Param> params){
        System.out.println(params.toString());
        R r = new R();
        r.setName("map");
        return r;
    }

    @Data
    public static class Param{
        private String name;
        private Integer age;
        private List<Param> params;
    }

    @Data
    public static class R{
        String name;
    }



}
