package com.gitee.starblues.example.codegenerator;

import com.baomidou.mybatisplus.annotation.DbType;
import org.junit.jupiter.api.Test;

/**
 * mybatis plus代码生成
 * @author starBlues
 * @version 1.0
 */
public class CodeGeneratorTest {

    private String driverName = "com.mysql.cj.jdbc.Driver";
    private String dbUrl = "jdbc:mysql://127.0.0.1:3306/plugin-test-example?" +
            "useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC";

    private String dbUsername = "root";
    private String dbPassword = "123456";
    private DbType dbType = DbType.MYSQL;
    private String author = "starBlues";
    private String outputDir = "D:\\example";


    private CodeGenerator generate = new CodeGenerator(driverName, dbUrl,
            dbUsername, dbPassword, dbType,
            author, outputDir);


    @Test
    public void generateMain(){
        generate.generateByTables("com.gitee.starblues.example", true,
                "main_role", "main_user", "main_user_role");
    }

    @Test
    public void generatePlugin1(){
        generate.generateByTables("com.gitee.starblues.example.plugin1", true,
                "plugin1_role", "plugin1_user", "plugin1_user_role");
    }

    @Test
    public void generatePlugin2(){
        generate.generateByTables("com.gitee.starblues.example.plugin2", true,
                "plugin2_role", "plugin2_user", "plugin2_user_role");
    }
}
