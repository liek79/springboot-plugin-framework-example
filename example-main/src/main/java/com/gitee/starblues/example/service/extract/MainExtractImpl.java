package com.gitee.starblues.example.service.extract;

import com.gitee.starblues.annotation.Extract;
import com.gitee.starblues.example.service.TestAnnotation;

/**
 * @author starBlues
 * @version 1.0
 */
@Extract(bus = "extract", scene = "demo", useCase = "main", order = Integer.MAX_VALUE)
@TestAnnotation
public class MainExtractImpl implements ExtractDemo{
    @Override
    public String getName() {
        return "mainExtractImpl";
    }
}
