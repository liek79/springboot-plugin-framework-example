package com.gitee.starblues.example.service;

import java.lang.annotation.*;

/**
 * 测试注解
 * @author starBlues
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TestAnnotation {



}
