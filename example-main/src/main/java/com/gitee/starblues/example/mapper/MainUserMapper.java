package com.gitee.starblues.example.mapper;

import com.gitee.starblues.example.entity.MainUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 主程序-用户表 Mapper 接口
 * </p>
 *
 * @author starBlues
 * @since 2021-05-21
 */
public interface MainUserMapper extends BaseMapper<MainUser> {

}
