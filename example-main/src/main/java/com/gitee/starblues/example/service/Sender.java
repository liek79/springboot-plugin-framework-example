package com.gitee.starblues.example.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @author starBlues
 * @version 1.0
 */
public interface Sender {

    Name getName();

    @AllArgsConstructor
    @Getter
    class Name{
        private String name;
    }


}
