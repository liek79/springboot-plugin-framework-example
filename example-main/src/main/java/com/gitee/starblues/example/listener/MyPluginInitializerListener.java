package com.gitee.starblues.example.listener;

import com.gitee.starblues.integration.listener.PluginInitializerListener;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
public class MyPluginInitializerListener implements PluginInitializerListener {
    @Override
    public void before() {
        System.out.println("初始化之前");
    }

    @Override
    public void complete() {
        System.out.println("初始化完成");
    }

    @Override
    public void failure(Throwable throwable) {
        System.out.println("初始化失败:"+throwable.getMessage());
    }
}
