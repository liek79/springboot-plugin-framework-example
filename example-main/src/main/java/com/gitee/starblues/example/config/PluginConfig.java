//package com.gitee.starblues.example.config;
//
//import com.gitee.starblues.loader.classloader.DefaultMainResourcePatternDefiner;
//import com.gitee.starblues.core.classloader.EmptyMainResourcePatternDefiner;
//import com.gitee.starblues.core.classloader.MainResourcePatternDefiner;
//import com.gitee.starblues.example.listener.Pf4jPluginListener;
//import com.gitee.starblues.extension.log.SpringBootLogExtension;
//import com.gitee.starblues.extension.mybatis.SpringBootMybatisExtension;
//import com.gitee.starblues.extension.resources.StaticResourceExtension;
//import com.gitee.starblues.integration.AutoIntegrationConfiguration;
//import com.gitee.starblues.integration.IntegrationConfiguration;
//import com.gitee.starblues.integration.application.AutoPluginApplication;
//import com.gitee.starblues.integration.application.PluginApplication;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//
///**
// * 插件配置
// * @author starBlues
// * @version 1.0
// */
//@Configuration
////@Import(AutoIntegrationConfiguration.class)
//public class PluginConfig {
//
//    /**
//     * 定义插件应用。使用可以注入它操作插件。
//     * @return PluginApplication
//     */
////    @Bean
////    public PluginApplication pluginApplication(){
////        // 实例化自动初始化插件的PluginApplication
////        PluginApplication pluginApplication = new AutoPluginApplication();
////        pluginApplication.addPf4jStateListener(Pf4jPluginListener.class);
////        // 新增mybatis-plus扩展
////        pluginApplication
////                .addExtension(new SpringBootMybatisExtension(SpringBootMybatisExtension.Type.MYBATIS_PLUS))
////                .addExtension(new SpringBootLogExtension(SpringBootLogExtension.Type.LOGBACK))
////                .addExtension(new StaticResourceExtension(StaticResourceExtension.Include.THYMELEAF));
////
////        return pluginApplication;
////    }
//
//}
