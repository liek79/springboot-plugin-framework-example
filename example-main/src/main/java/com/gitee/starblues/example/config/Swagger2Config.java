package com.gitee.starblues.example.config;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.*;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.function.Predicate;

/**
 * Swagger2 配置
 * 访问地址：http:127.0.0.1:port/doc.html
 * 文档说明：https://doc.xiaominfo.com/knife4j/
 * @author starBlues
 * @version 1.0
 */
@Configuration
@EnableSwagger2WebMvc
public class Swagger2Config {

    @Value("${server.port}")
    private String port;


    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Predicate<String>  predicate = PathSelectors.any();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("dev")
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(predicate)
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("接口文档")
                .description("接口说明文档")
                .termsOfServiceUrl("http://ip:" + port + "/**")
                .contact(new Contact("starBlues", "", ""))
                .version("1.0.0-SNAPSHOT")
                .build();
    }

}
