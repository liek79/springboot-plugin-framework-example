package com.gitee.starblues.example.service.extract;

/**
 * 扩展接口
 * @author starBlues
 * @version 1.0
 */
public interface ExtractDemo {

    /**
     * getName
     * @return String
     */
    String getName();

}
