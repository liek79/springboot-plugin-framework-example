package com.gitee.starblues.example.rest;

import com.gitee.starblues.example.service.MainSender;
import com.gitee.starblues.example.service.Sender;
import com.gitee.starblues.example.service.TestAnnotation;
import com.gitee.starblues.integration.application.PluginApplication;
import com.gitee.starblues.integration.user.BeanWrapper;
import com.gitee.starblues.integration.user.PluginUser;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zhangzhuo@acoinfo.com
 * @version 1.0
 * @date 2021-10-08
 */
@RestController
@RequestMapping("/plugin-user")
@Api(tags = "main-plugin-user")
public class PluginUserController {

    @Autowired
    private PluginUser pluginUser;

    @GetMapping("/bean-names")
    public BeanWrapper<Set<String>> getBeanName(){
        return pluginUser.getBeanName(true);
    }

    @GetMapping("/name")
    public Object getBean(@RequestParam("name") String name,
                          @RequestParam("includeMainBeans") Boolean includeMainBeans){
        return pluginUser.getBean(name, includeMainBeans);
    }

    @GetMapping("/name-by-plugin")
    public Object getBean(@RequestParam("pluginId") String pluginId,
                          @RequestParam("name") String name){
        return pluginUser.getBean(pluginId, name);
    }

    @GetMapping("/by-interface")
    public BeanWrapper<List<Sender>> getBeanByInterface(@RequestParam("includeMainBeans") Boolean includeMainBeans){
        return pluginUser.getBeanByInterface(Sender.class, includeMainBeans);
    }

    @GetMapping("/by-interface-error")
    public String getBeanByInterfaceError(){
        try {
            pluginUser.getBeanByInterface(MainSender.class, true);
            return "ok";
        } catch (Exception e){
            return e.getMessage();
        }
    }

    @GetMapping("/interface-of-plugin")
    public List<String> getBeanByInterface(@RequestParam("pluginId") String pluginId){
        List<Sender> senders = pluginUser.getBeanByInterface(pluginId, Sender.class);
        return senders.stream().map(o -> o.getName().getName()).collect(Collectors.toList());
    }


    @GetMapping("/with-annotation")
    public BeanWrapper<List<Object>> getObjectWithAnnotation(@RequestParam("includeMainBeans") Boolean includeMainBeans){
        return pluginUser.getBeansWithAnnotation(TestAnnotation.class, includeMainBeans);
    }


    @GetMapping("/with-annotation-of-plugin")
    public List<String> getObjectWithAnnotationByPluginId(@RequestParam("pluginId") String pluginId){
        List<Object> objects = pluginUser.getBeansWithAnnotation(pluginId, TestAnnotation.class);
        return objects.stream().map(o -> o.getClass().getName()).collect(Collectors.toList());
    }

}
