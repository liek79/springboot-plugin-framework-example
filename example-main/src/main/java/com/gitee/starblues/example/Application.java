package com.gitee.starblues.example;

import com.gitee.starblues.loader.launcher.SpringMainBootstrap;
import com.gitee.starblues.loader.launcher.SpringBootstrap;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author starBlues
 * @version 1.0
 */
@SpringBootApplication
@MapperScan("com.gitee.starblues.example.mapper")
public class Application implements SpringBootstrap {

    public static void main(String[] args) {
        SpringMainBootstrap.launch(Application.class, args);
    }

    @Override
    public void run(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
