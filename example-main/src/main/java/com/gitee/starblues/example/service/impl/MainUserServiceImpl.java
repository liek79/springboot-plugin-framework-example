package com.gitee.starblues.example.service.impl;

import com.gitee.starblues.example.entity.MainUser;
import com.gitee.starblues.example.mapper.MainUserMapper;
import com.gitee.starblues.example.service.MainUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 主程序-用户表 服务实现类
 * </p>
 *
 * @author starBlues
 * @since 2021-05-21
 */
@Service
public class MainUserServiceImpl extends ServiceImpl<MainUserMapper, MainUser> implements MainUserService {

}
