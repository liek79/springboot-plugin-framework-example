package com.gitee.starblues.example.service;

import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
@TestAnnotation
public class MainSender implements Sender{
    @Override
    public Name getName() {
        return new Name(this.getClass().getName());
    }
}
