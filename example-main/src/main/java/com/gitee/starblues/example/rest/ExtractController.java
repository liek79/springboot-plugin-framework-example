package com.gitee.starblues.example.rest;

import com.gitee.starblues.example.service.extract.ExtractDemo;
import com.gitee.starblues.spring.extract.ExtractCoordinate;
import com.gitee.starblues.spring.extract.ExtractFactory;
import com.gitee.starblues.utils.ObjectUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author starBlues
 * @version 1.0
 * @since 2021-07-24
 */
@RestController
@RequestMapping("/extract")
@Api(tags = "main-extract")
public class ExtractController {

    @Autowired
    private ExtractFactory extractFactory;


    @GetMapping("/getExtractByCoordinate")
    public String getExtractImpl(@RequestParam("bus") String bus,
                                 @RequestParam(value = "scene", required = false) String scene,
                                 @RequestParam(value = "useCase", required = false) String useCase){
        ExtractDemo extract = extractFactory.getExtractByCoordinate(ExtractCoordinate.build(bus, scene, useCase));
        return extract.getName();
    }

    @GetMapping("/getExtractByInterClass")
    public List<String> getExtractByInterClass(@RequestParam(value = "pluginId", required = false) String pluginId){
        List<ExtractDemo> extractByInterClass = null;
        if(ObjectUtils.isEmpty(pluginId)){
            extractByInterClass = extractFactory.getExtractByInterClass(ExtractDemo.class);
        } else {
            extractByInterClass = extractFactory.getExtractByInterClass(pluginId, ExtractDemo.class);
        }
        return extractByInterClass.stream()
                .map(ExtractDemo::getName)
                .collect(Collectors.toList());
    }

    @GetMapping("/getExtractByInterClassOfMain")
    public List<String> getExtractByInterClassOfMain(){
        List<ExtractDemo> extractByInterClass = extractFactory.getExtractByInterClassOfMain(ExtractDemo.class);
        return extractByInterClass.stream()
                .map(ExtractDemo::getName)
                .collect(Collectors.toList());
    }


}
