package com.gitee.starblues.example.service;

import com.gitee.starblues.example.entity.MainUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 主程序-用户表 服务类
 * </p>
 *
 * @author starBlues
 * @since 2021-05-21
 */
public interface MainUserService extends IService<MainUser> {

}
