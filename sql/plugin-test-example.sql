/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 80015
Source Host           : 127.0.0.1:3306
Source Database       : plugin-test-example

Target Server Type    : MYSQL
Target Server Version : 80015
File Encoding         : 65001

Date: 2021-07-24 08:45:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for main_user
-- ----------------------------
DROP TABLE IF EXISTS `main_user`;
CREATE TABLE `main_user` (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名称',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码(加密后)',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '帐号状态（1启用, 0停用）',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '删除标记（1删除, 0 正常）',
  `locked` int(6) NOT NULL DEFAULT '0' COMMENT '是否被锁(小于等于5表示未被锁, 大于5表示被锁)',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户',
  `gmt_created` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `modified_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改用户',
  `gmt_modified` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改时间',
  `avatar` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像地址',
  `last_login_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上一次登录的ip地址',
  `last_gmt_login_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上一次登录的时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='主程序-用户表';

-- ----------------------------
-- Records of main_user
-- ----------------------------
INSERT INTO `main_user` VALUES ('1', 'admin', '16666666666', '123@qq.com', 'admin', '$2a$10$IqjSzOzTpMX79DnxS7aBz.A1.uqR.DvuvMtx9TaGmwpsxgJDFW8wS', '1', '0', '0', 'admin', '2021-01-01 08:08:08', 'admin', 'admin', '2e418339355940a9b83f72d597a6da2c.jpg', '127.0.0.1', '2021-02-02 20:14:21');

-- ----------------------------
-- Table structure for plugin1_user
-- ----------------------------
DROP TABLE IF EXISTS `mp_plugin_user`;
CREATE TABLE `mp_plugin_user` (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名称',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码(加密后)',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '帐号状态（1启用, 0停用）',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '删除标记（1删除, 0 正常）',
  `locked` int(6) NOT NULL DEFAULT '0' COMMENT '是否被锁(小于等于5表示未被锁, 大于5表示被锁)',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户',
  `gmt_created` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `modified_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改用户',
  `gmt_modified` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改时间',
  `avatar` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像地址',
  `last_login_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上一次登录的ip地址',
  `last_gmt_login_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上一次登录的时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='插件1-用户表';

-- ----------------------------
-- Records of plugin1_user
-- ----------------------------
INSERT INTO `mp_plugin_user` VALUES ('1', '1', '1', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `mp_plugin_user` VALUES ('ca49e3f196c428723f31e06b77731e6b', 'name-1', null, null, 'plugin1-1', '1', '1', '0', '2', 'admin', '2021-07-24 08:38:40', 'admin', '2021-07-24 08:38:40', null, null, null);

-- ----------------------------
-- ----------------------------
-- Table structure for plugin_jpa_user
-- ----------------------------
DROP TABLE IF EXISTS `plugin_jpa_user`;
CREATE TABLE `plugin_jpa_user` (
  `user_id` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gmt_created` varchar(255) DEFAULT NULL,
  `gmt_modified` varchar(255) DEFAULT NULL,
  `last_gmt_login_time` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `locked` int(11) DEFAULT NULL,
  `modified_user` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;