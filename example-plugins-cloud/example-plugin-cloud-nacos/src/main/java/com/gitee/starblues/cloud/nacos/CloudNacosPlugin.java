package com.gitee.starblues.cloud.nacos;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;

/**
 * @author starBlues
 * @version 1.0
 */
@SpringBootApplication(exclude = JacksonAutoConfiguration.class)
public class CloudNacosPlugin extends SpringPluginBootstrap {

    public static void main(String[] args) {
        new CloudNacosPlugin().run(args);
    }

}
