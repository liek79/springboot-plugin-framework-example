package com.gitee.starblues.cloud.nacos.config;

import com.alibaba.nacos.api.config.ConfigType;
import com.alibaba.nacos.api.config.annotation.NacosConfigurationProperties;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Data
@Component
@NacosConfigurationProperties(prefix = "nacos-config", dataId = "example.yaml", type = ConfigType.YAML, autoRefreshed = true)
public class NacosConfigExample {

    private String name;

    private Integer age;

}
