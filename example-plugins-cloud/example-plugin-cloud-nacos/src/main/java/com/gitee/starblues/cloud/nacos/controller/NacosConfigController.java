package com.gitee.starblues.cloud.nacos.controller;

import com.gitee.starblues.cloud.nacos.config.NacosConfigExample;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author starBlues
 * @version 1.0
 */
@RestController
@RequestMapping("nacos-config")
@Api(tags = "nacos-plugin-config")
public class NacosConfigController {

    @Autowired
    private NacosConfigExample configExample;

    @GetMapping
    public NacosConfigExample getConfig() {
        return configExample;
    }

}
