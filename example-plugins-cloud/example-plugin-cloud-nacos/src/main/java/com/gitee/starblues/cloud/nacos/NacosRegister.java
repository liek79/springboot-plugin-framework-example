package com.gitee.starblues.cloud.nacos;

import com.alibaba.boot.nacos.discovery.autoconfigure.NacosDiscoveryAutoRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author starBlues
 * @version 1.0
 */
@Component
public class NacosRegister implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private NacosDiscoveryAutoRegister register;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        register.onApplicationEvent(null);
    }
}
