REM windows package

REM package
::如果不需要进行maven打包, 则使用 REM 注释掉该行
call mvn clean install -Dmaven.test.skip=true -P prod

REM del dist
rmdir dist /s /q

REM create dist
mkdir dist
mkdir dist\plugins

REM copy main program and config
xcopy example-main\target\example-main-*-repackage.jar dist /s /i
xcopy example-main\src\main\resources\application-prod.yml dist /s

xcopy example-main\target\lib\* dist\lib /y /e /i /q

REM copy plugin and config
xcopy example-plugins-basic\example-basic-1\target\*-repackage.jar dist\plugins /s
xcopy example-plugins-basic\example-basic-2\target\*-repackage.jar dist\plugins /s

xcopy example-plugins-db\example-jpa\target\*-repackage.jar dist\plugins /s
xcopy example-plugins-db\example-mybatis-plus\target\*-repackage.jar dist\plugins /s

::xcopy example-plugins-cloud\example-plugin-cloud-nacos\target\*-repackage.jar dist\plugins /s

REM copy bin
xcopy bin\* dist\ /s

cd dist

REM run main
rename example-main-*-repackage.jar example.jar
rename application-prod.yml application.yml

start explorer .

:: 运行命令为: java -jar example.jar --spring.config.location=application.yml